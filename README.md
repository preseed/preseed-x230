# x230 Preseed

This file will help install Debian on my (Coreboot) Lenovo x230.

I didn't bother with configuring Networking because networking is set up
before the preseed is used since I host it over the network.

Speaking of networking, I connected the laptop with a CAT5 cable to perform the installation.

The only interaction this requires, aside from pointing the installer to this
preseed file, is entering the encryption password.

I selected 'atomic' partition option to have everything in one partition. 
In the past I have set up different partitions for /, /var, /home, /tmp etc.. 
but after a few years, it seems kind of unnecessary for my personal computer.

It installs a few extra applications, sets Vim as the default editor,
and expires my password. I will enter the new password at first login then 
continue setting up the OS.
